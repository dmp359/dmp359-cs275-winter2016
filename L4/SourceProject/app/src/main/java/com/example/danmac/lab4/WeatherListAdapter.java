package com.example.danmac.lab4;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danmac.lab4.R;
import com.example.danmac.lab4.Weather;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class WeatherListAdapter extends ArrayAdapter<Weather> {
    //helper
    private String convertMilitary(String s){

        int hour = Integer.parseInt(s);

        if (hour < 13) {
            if (hour > 0)
                return s + " AM";
            else if (hour ==0) //fix "0 AM"
                return "12 AM";
        }
        else {
            return (hour - 12) + " PM";
        }

        //never hit
        return "";
    }

    Context context;
    int layoutResourceId;
    ArrayList<Weather>data = null;
    private static final int IO_BUFFER_SIZE = 4 * 1024;


    public WeatherListAdapter(Context context, int textViewResourceId, ArrayList<Weather> items) {
        super(context, textViewResourceId, items);
        this.context=context;
        this.layoutResourceId = textViewResourceId;
        this.data = items;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        WeatherHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new WeatherHolder();


           // holder.imgIcon = (ImageView)row.findViewById(R.id.imgIcon);
            holder.description = (TextView)row.findViewById(R.id.txtDesc);
            holder.date = (TextView)row.findViewById(R.id.txtDate);
            holder.hour = (TextView)row.findViewById(R.id.txtHour);
            holder.temp = (TextView)row.findViewById(R.id.txtTemp);
            holder.humidity = (TextView)row.findViewById(R.id.txtHumidity);

            row.setTag(holder);
        }
        else
        {
            holder = (WeatherHolder)row.getTag();
        }


        Weather weather = data.get(position);

        holder.description.setText(weather.description);
        holder.date.setText(" " + weather.date);
        String usHour = convertMilitary(weather.hour);
        holder.hour.setText(usHour + " | ");
        holder.temp.setText(weather.temp+"°F");
        holder.humidity.setText("Humidity: " + weather.humidity + "%");
       //fix for image-!!!!!
        ImageView icon = (ImageView) row.findViewById(R.id.imgIcon);

        new ImageDownloader(icon).execute(weather.imgURL);

        return row;
    }




    static class WeatherHolder
    {
        ImageView imgIcon;
        TextView description, date, hour, temp, humidity;
    }



}