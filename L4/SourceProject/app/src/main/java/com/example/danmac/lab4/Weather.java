package com.example.danmac.lab4;

/**
 * Created by DanMac on 2/26/16.
 */
public class Weather {

    public String date, hour, description, temp, humidity,imgURL;

    Weather() {
        super();
    }

    Weather(String _temp,  String _humidity, String _date, String _hour, String _description, String _imgURL){
        super();
        temp=_temp;
        humidity=_humidity;
        date=_date;
        hour=_hour;
        description=_description;
        imgURL=_imgURL;

    }
}
