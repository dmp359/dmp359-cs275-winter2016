package com.example.danmac.lab4;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private static final String key = "dc03cb46f317b319";
    private SQLiteDatabase forecasts;
    private String provider;
    private LocationManager manager = null;

    private GoogleApiClient client;

    //callback for the request task
    public interface Callback {
        void finished(JsonObject result);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create database object
        forecasts = openOrCreateDatabase("Weather", Context.MODE_PRIVATE, null);

        //create the weather table
        forecasts.execSQL("CREATE TABLE IF NOT EXISTS weather(id VARCHAR,json VARCHAR);");

        //get the location manager
        manager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Get the last known location
        Criteria criteria = new Criteria();
        provider = manager.getBestProvider(criteria, false);


        //required boiler plate for next line
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        //end required
        Location locationManager = manager.getLastKnownLocation(provider);

        //check if location exists
        if (locationManager != null) {
            LatitudeLongitude location = new LatitudeLongitude();
            location.latit = locationManager.getLatitude();
            location.longit = locationManager.getLongitude();

            //get the hourly forecast
            getHourly(location);
        } else {
            //geoIP
            getGeoIP(new Callback() {

                public void finished(JsonObject result) {
                    LatitudeLongitude location = new LatitudeLongitude();
                    result = result.get("location").getAsJsonObject();
                    location.latit = result.get("lat").getAsDouble();
                    location.longit = result.get("lon").getAsDouble();


                    getHourly(location);
                }
            });
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    void getHourly(LatitudeLongitude loc) {
      //  Log.d("lat: ", String.valueOf(loc.latit));
      //  Log.d("long: ", String.valueOf(loc.longit));
        SharedPreferences prefs = this.getSharedPreferences("dan.perlman.weather", Context.MODE_PRIVATE);

        //here..?
        //if less than an hour has passed
        if (new Date().getTime() - prefs.getLong("time", 0) < 3600000) {
            //select everything from entry
            Cursor c = forecasts.rawQuery("SELECT * FROM weather WHERE id = '1'", null);
            if (c.moveToFirst()) {
                //parse result
                JsonObject result = new JsonParser().parse(c.getString(1)).getAsJsonObject();
                JsonArray forecastArray = result.get("hourly_forecast").getAsJsonArray();

                //weather object list
                ArrayList<Weather> weather_data = new ArrayList<Weather>();

                for (int i=0; i<forecastArray.size(); i++) {
                    //get fcctime object
                    JsonObject fcttime = forecastArray.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject();

                    //get strings for date and time
                    String hour = fcttime.get("hour").getAsString();
                    //compile date
                    String month = fcttime.get("mon_abbrev").getAsString();
                    String day = fcttime.get("weekday_name_abbrev").getAsString();
                    String dateDay = fcttime.get("mday_padded").getAsString();

                    String date = day + " " + month + " " + dateDay ;


                    String description = forecastArray.get(i).getAsJsonObject().get("condition").getAsString();
                    String humidity = forecastArray.get(i).getAsJsonObject().get("humidity").getAsString();
                    String temperature = forecastArray.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();

                    String imageURL = forecastArray.get(i).getAsJsonObject().get("icon_url").getAsString();


                    Weather w = new Weather(temperature, humidity, date, hour, description, imageURL);



                    weather_data.add(w);

                }


                WeatherListAdapter adapter = new WeatherListAdapter(MainActivity.this,
                        R.layout.listview_item_row, weather_data);



                ListView listView1 = (ListView) findViewById(R.id.listData);
                View header = (View)getLayoutInflater().inflate(R.layout.listview_header_row, null);
                listView1.addHeaderView(header);


                listView1.setAdapter(adapter);


                return;
            }
        }

        //save date and time for next call
        Date dt = new Date();
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("time", dt.getTime()).apply();
        editor.apply();

    //now we have lat and long, use it to get weather :)
        String sURL = "http://api.wunderground.com/api/" + key + "/hourly/q/"+loc.latit+","+loc.longit+".json";

        //perform get request in background (async)
        new RequestTask(MainActivity.this, new Callback() {
            @Override
            public void finished(JsonObject result) {
                if (exists("weather", "id", "1"))
                    forecasts.execSQL("UPDATE weather SET json='" + result.toString() + "' WHERE id ='1'");
                else
                    forecasts.execSQL("INSERT INTO weather VALUES('1', '" + result.toString() + "');");

                //go through result
                JsonArray forecastsArray = result.get("hourly_forecast").getAsJsonArray();

                String imageURL = "http://icons.wxug.com/i/c/k/nt_clear.gif";

                //weather object list
                ArrayList<Weather> weather_data = new ArrayList<Weather>();

                for (int i=0; i<forecastsArray.size(); i++) {
                    //get fcctime object
                    JsonObject fcttime = forecastsArray.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject();

                    //get strings for date and time
                    String hour = fcttime.get("hour").getAsString();
                    //compile date
                    String month = fcttime.get("mon_abbrev").getAsString();
                    String day = fcttime.get("weekday_name_abbrev").getAsString();
                    String dateDay = fcttime.get("mday_padded").getAsString();
                    String date = day + " " + month + " " + dateDay ;


                    String description = forecastsArray.get(i).getAsJsonObject().get("condition").getAsString();
                    String humidity = forecastsArray.get(i).getAsJsonObject().get("humidity").getAsString();
                    String temperature = forecastsArray.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();


                     imageURL = forecastsArray.get(i).getAsJsonObject().get("icon_url").getAsString();

                    Weather w = new Weather(temperature, humidity, date, hour, description, imageURL);

                    weather_data.add(w);

                }


                WeatherListAdapter adapter = new WeatherListAdapter(MainActivity.this,
                        R.layout.listview_item_row, weather_data);


                ListView listView1 = (ListView) findViewById(R.id.listData);
                View header = (View)getLayoutInflater().inflate(R.layout.listview_header_row, null);
                listView1.addHeaderView(header);

                listView1.setAdapter(adapter);
               //image url to image




            }
        }).execute(sURL);
    }

    void getGeoIP(Callback call) {
        String hourlyURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";

        new RequestTask(MainActivity.this, call).execute(hourlyURL);
    }

    public boolean exists(String TableName, String dbfield, String fieldValue) {
        String Query = "Select * from " + TableName + " where " + dbfield + "='" + fieldValue + "'";
        Cursor cursor = forecasts.rawQuery(Query, null);
        if (cursor.moveToFirst())
            return true;
        //else
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.danmac.lab4/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.danmac.lab4/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    //get request
    public static class RequestTask extends AsyncTask<String, JsonObject, JsonObject> {
        Context context;
        Callback call;

        public RequestTask(Context c, Callback _call) {
            context = c;
            call = _call;
        }

        @Override
        protected JsonObject doInBackground(String... uri) {

            String path = uri[0];
            for (int i = 1; i < uri.length; i++)
                path += (i == 1 ? '?' : '&') + uri[i];
            try {
                URL url = new URL(path);
                URLConnection connect = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
                return new JsonParser().parse(in).getAsJsonObject();

            } catch (IOException e) {
                Toast.makeText(context, "Location not found.", Toast.LENGTH_SHORT).show();
                return null;
            }

        }

        @Override
        protected void onPostExecute(JsonObject result) {
            super.onPostExecute(result);
            call.finished(result);
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onResume() {
        super.onResume();
        //if user allows

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3600000, 1000, onLocationChange);
    }


    LocationListener onLocationChange = new LocationListener(){

        @Override
    public void onLocationChanged(Location location) {
        getHourly(new LatitudeLongitude(location.getLatitude(), location.getLongitude()));
    }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }


    };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





}







