package com.example.danmac.lab4;

/**
 * Created by DanMac on 2/26/16.
 */
//Get coordinates
public class LatitudeLongitude {
    public double latit;
    public double longit;

    LatitudeLongitude() {
        latit = 0;
        longit = 0;
    }

    LatitudeLongitude(double lat, double lon) {
        latit = lat;
        longit = lon;
    }
}

