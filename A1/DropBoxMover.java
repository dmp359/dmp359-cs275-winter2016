import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.UploadFile.UploadFileResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class DropBoxMover {
	
public static int getLengthBeforeSpace(String s){
		
		int counter=0;
		for (int i=0; i<s.length(); i++)
		{
			if (s.charAt(i) == ' ')
				break;
			else
				counter++;
			
		}
		
		return counter;
	}
		
	
	public static void main(String[] args) throws TembooException, IOException {
		// TODO Auto-generated method stub
		
		String acctName, appKeyName, appKeyValue;
		String client_id, redirect_uri, code, client_secret, access_token;
		String dropKey,dropSecret;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Entered Temboo Account Name");
		acctName="dmp359";
		System.out.println("Entered Temboo App Key Name");
		appKeyName = "dropboxMover";
		System.out.println("Entered Temboo App Key Value");
		appKeyValue = "aoqVhGI625u26Y0m1aQQQbNjBvtEXpR1";
		System.out.println("Entered Dropbox App key");
		dropKey="huzbhfso0exmwdg";
		System.out.println("Entered Dropbox secret");
		dropSecret="g230nwftwgls0js";
		
		// Instantiate the Choreo, using a previously instantiated TembooSession object, eg:
		TembooSession session = new TembooSession(acctName, appKeyName, appKeyValue);

		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs of Dropbox data
		initializeOAuthInputs.set_DropboxAppKey(dropKey);
		initializeOAuthInputs.set_DropboxAppSecret(dropSecret);
		
		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		String oAuthURL = initializeOAuthResults.get_AuthorizationURL();
		String callbackID = initializeOAuthResults.get_CallbackID();
		String oAuthToken = initializeOAuthResults.get_OAuthTokenSecret();
		
		
		System.out.println("Go here and click allow: \n"+ oAuthURL);
		//Finalize the choreo----------------------------
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_DropboxAppKey(dropKey);
		finalizeOAuthInputs.set_DropboxAppSecret(dropSecret);
		finalizeOAuthInputs.set_CallbackID(callbackID);
		finalizeOAuthInputs.set_OAuthTokenSecret(oAuthToken);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		//Now we have the access token and secret 
		String accessToken = finalizeOAuthResults.get_AccessToken();
		String tokenSecret = finalizeOAuthResults.get_AccessTokenSecret();
		
		//--------Getting dropbox files
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();

		// Set inputs
		getFileInputs.set_AccessToken(accessToken);
		getFileInputs.set_AccessTokenSecret(tokenSecret);
		getFileInputs.set_AppKey(dropKey);
		getFileInputs.set_AppSecret(dropSecret);
		//get _list file
		getFileInputs.set_Path("/move/_list.txt");
		//optional input. do not encode file content
		getFileInputs.setInput("EncodeFileContent", false);
		
		// Execute Choreo
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
		String output = getFileResults.get_Response();
		
		//print each file to be moved
		System.out.println(output);
		
		//Get file names/locations
		List<String> files = new ArrayList<String>();
		List<String> locations = new ArrayList<String>();

		//loop through the lines of the output string and get the files and locations
		BufferedReader bufReader = new BufferedReader(new StringReader(output));
		String line=null;
		while ( (line=bufReader.readLine()) != null )
		{
			int spacePos= getLengthBeforeSpace(line);
			String fileName = line.substring(0, spacePos);
			files.add(fileName);
			String fileLocation = line.substring(spacePos+1, line.length());
			locations.add(fileLocation+"/"+fileName);
		}
		System.out.println("Number of files to be moved: " + files.size());
		
		
		//Copy each file. loop over the files/location array
		for (int i=0; i<files.size(); i++)
		{
		  
			//get each file from dropbox
			  GetFile getFileChoreo2 = new GetFile(session);

				// Get an InputSet object for the choreo
				GetFileInputSet getFileInputs2 = getFileChoreo2.newInputSet();

				// Set inputs
				getFileInputs2.set_AccessToken(accessToken);
				getFileInputs2.set_AccessTokenSecret(tokenSecret);
				getFileInputs2.set_AppKey(dropKey);
				getFileInputs2.set_AppSecret(dropSecret);
				getFileInputs2.set_Path("/move/"+files.get(i));
				//do not encode file content
				getFileInputs2.setInput("EncodeFileContent", false);
				
				// Execute Choreo
				GetFileResultSet getFileResults2 = getFileChoreo2.execute(getFileInputs2);
				String fileContents = getFileResults2.get_Response();
				
				//Write this files contents to a new file 
				PrintWriter writer = new PrintWriter(files.get(i), "UTF-8");
				writer.println(fileContents);
				writer.close();
				//Move this file from local java location to desired location
				File currentFile = new File("/Users/DanMac/Documents/JavaWorkspace/DropboxMover/"+files.get(i));
				File desiredLocationFile = new File(locations.get(i));
				//move it (side effect of rename)
				currentFile.renameTo(desiredLocationFile);
		  
		
		}
		//Files are now copied
		//Clear out input file (delete it and re-add it)
		DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);

		// Get an InputSet object for the choreo
		DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();

		// Set inputs
		deleteFileOrFolderInputs.set_AccessToken(accessToken);
		deleteFileOrFolderInputs.set_AccessTokenSecret(tokenSecret);
		deleteFileOrFolderInputs.set_AppKey(dropKey);
		deleteFileOrFolderInputs.set_AppSecret(dropSecret);
		deleteFileOrFolderInputs.set_Path("move/_list.txt");

		// Execute Choreo
		DeleteFileOrFolderResultSet deleteFileOrFolderResults = deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);
		
		//add _list.txt back
		UploadFile uploadFileChoreo = new UploadFile(session);

		// Get an InputSet object for the choreo
		UploadFileInputSet uploadFileInputs = uploadFileChoreo.newInputSet();

		// Set inputs
		uploadFileInputs.set_AccessToken(accessToken);
		uploadFileInputs.set_AccessTokenSecret(tokenSecret);
		uploadFileInputs.set_AppKey(dropKey);
		uploadFileInputs.set_AppSecret(dropSecret);		
		uploadFileInputs.set_FileContents(" ");
		uploadFileInputs.set_FileName("_list.txt");
		uploadFileInputs.set_Folder("/move");
		
		// Execute Choreo
		UploadFileResultSet uploadFileResults = uploadFileChoreo.execute(uploadFileInputs);

	}
}
