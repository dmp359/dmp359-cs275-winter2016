package com.example.danmac.whosaround;

/**
 * Created by DanMac on 2/26/16.
 */
public class Friend {

    public String name, day, hr, minute, email, distance,lat,lon;

    Friend() {
        super();
    }

    Friend(String _name, String _day,  String _hr, String _minute, String _email, String _distance, String _lat, String _lon){
        super();
        day=_day;
        hr=_hr;
        minute=_minute;
        email=_email;
        distance=_distance;
        lat=_lat;
        lon = _lon;
        name = _name;

    }
}
