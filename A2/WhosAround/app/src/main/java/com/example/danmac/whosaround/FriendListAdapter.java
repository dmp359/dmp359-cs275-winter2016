package com.example.danmac.whosaround;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danmac.whosaround.R;
import com.example.danmac.whosaround.Friend;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class FriendListAdapter extends ArrayAdapter<Friend> {


    Context context;
    int layoutResourceId;
    ArrayList<Friend> data = null;
    private static final int IO_BUFFER_SIZE = 4 * 1024;


    public FriendListAdapter(Context context, int textViewResourceId, ArrayList<Friend> items) {
        super(context, textViewResourceId, items);
        this.context=context;
        this.layoutResourceId = textViewResourceId;
        this.data = items;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        FriendHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new FriendHolder();


            holder.time = (TextView)row.findViewById(R.id.txtTime);
            holder.email = (TextView)row.findViewById(R.id.txtEmail);
            holder.distance = (TextView)row.findViewById(R.id.txtDistance);
            holder.name = (TextView)row.findViewById(R.id.txtName);
            row.setTag(holder);
        }
        else
        {
            holder = (FriendHolder)row.getTag();
        }


        Friend friend = data.get(position);


            holder.time.setText(friend.hr + ":" + friend.minute);
            holder.email.setText(friend.email);
            holder.name.setText(friend.name);
        double distDouble=Double.parseDouble(friend.distance);
        DecimalFormat df = new DecimalFormat("##.##");
        String shortenedDistance = (df.format(distDouble));
            holder.distance.setText(shortenedDistance + " miles away");


        return row;
    }




    static class FriendHolder
    {
        TextView name, time, email, distance;
    }



}