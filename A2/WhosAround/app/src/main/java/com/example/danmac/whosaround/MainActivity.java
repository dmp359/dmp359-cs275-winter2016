package com.example.danmac.whosaround;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.cloudmine.api.CMApiCredentials;
import com.cloudmine.api.CMObject;
import com.cloudmine.api.SimpleCMObject;
import com.cloudmine.api.rest.CMStore;
import com.cloudmine.api.rest.callbacks.CMObjectResponseCallback;
import com.cloudmine.api.rest.callbacks.ObjectModificationResponseCallback;
import com.cloudmine.api.rest.response.CMObjectResponse;
import com.cloudmine.api.rest.response.ObjectModificationResponse;

import static android.util.Log.*;

public class MainActivity extends Activity {

    private ImageView mapImage;
    private Button searchButton;
    private ListView lv;
    private Button mapButton;
    private String tag = "MainActivity";
    public String markerString="";
    public String curLocString="";
    public ArrayList<Friend> friend_data = new ArrayList<Friend>();

    private static final String APP_ID = "bbf6ec7b29d8404d511a5bcda4f2343e";
    // Find this in your developer console
    private static final String API_KEY = "bf9d7cc357754504a69fdd6edfb71657";

   // public static final String gmailpassword = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize CloudMine library
        CMApiCredentials.initialize(APP_ID, API_KEY, getApplicationContext());

        mapImage = (ImageView)findViewById(R.id.imageView1);
        searchButton = (Button)findViewById(R.id.search_button);


        /*
        //cloudmine friends
        SimpleCMObject friend1 = new SimpleCMObject();
        //friend 1
        friend1.add("name", "Joseph Urbano");
        friend1.add("locationLat", "39.9544775");
        friend1.add("locationLong", "-75.189241400000015");
        friend1.add("email", "dannyp789@gmail.com");
        friend1.add("hourAtLocation", "5");
        friend1.add("minuteAtLocation", "30");
        friend1.add("day", "0");
        friend1.save(new ObjectModificationResponseCallback() {
            public void onCompletion(ObjectModificationResponse response) {
                Log.d("friend1", "friend1 was saved: " + response.wasSuccess());
            }
        });

        SimpleCMObject friend2 = new SimpleCMObject();

        //friend 2
        friend2.add("name", "Dave Augenblick");
        friend2.add("locationLat", "39.953167");
        friend2.add("locationLong", "-75.189378");
        friend2.add("email", "dannyp789@gmail.com");
        friend2.add("hourAtLocation", "6");
        friend2.add("minuteAtLocation", "30");
        friend2.add("day", "0");

        friend2.save(new ObjectModificationResponseCallback() {
            public void onCompletion(ObjectModificationResponse response) {
                Log.d("friend2", "friend2 was saved: " + response.wasSuccess());
            }
        });

        //friend 3
        SimpleCMObject friend3 = new SimpleCMObject();

        friend3.add("name", "John Smith");
        friend3.add("locationLat", "39.9544");
        friend3.add("locationLong", "-75.2");
        friend3.add("email", "fake@gmail.com");
        friend3.add("hourAtLocation", "2");
        friend3.add("minuteAtLocation", "45");
        friend3.add("day", "1");

        friend3.save(new ObjectModificationResponseCallback() {
            public void onCompletion(ObjectModificationResponse response) {
                Log.d("friend3", "friend3 was saved: " + response.wasSuccess());
            }
        });

          */
        searchButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Location l = getLoc();


                //convert CM to friend list
                String searchQuery = "[day = \"0\"]"; //get friends that checked in today

                // use the default store for the application
                CMStore store = CMStore.getStore();

                store.loadApplicationObjectsSearch(searchQuery, new CMObjectResponseCallback() {
                    public void onCompletion(CMObjectResponse response) {
                        //show list

                        String cString = "";
                        String markers = "";

                        for (CMObject object : response.getObjects()) {
                            // only today's friends are returned
                            SimpleCMObject todayFriend = (SimpleCMObject) object;
                            //String _name, String _day,  String _hr, String _minute, String _email, String _distance, String _lat, String _lon)

                            String name = todayFriend.getString("name");
                            String locationLat = todayFriend.getString("locationLat");
                            String locationLong = todayFriend.getString("locationLong");
                            String email = todayFriend.getString("email");

                            String hourAtLocation = todayFriend.getString("hourAtLocation");
                            String minuteAtLocation = todayFriend.getString("minuteAtLocation");
                            String day = todayFriend.getString("day");

                            //get cur location to calculate distance and plot self marker

                            Location curLoc = getLoc();


                            try {
                                //make this current location
                                markerString = "&markers=color:red|" + URLEncoder.encode(String.valueOf(curLoc.getLatitude()) + "," + String.valueOf(curLoc.getLongitude()), "utf-8");

                            } catch (UnsupportedEncodingException e) {
                                return;
                            }

                            Location friendLoc = new Location("providername");
                            friendLoc.setLatitude(Double.parseDouble(locationLat));
                            friendLoc.setLongitude(Double.parseDouble(locationLong));

                            double distanceValue = coordinateToMiles(curLoc, friendLoc);

                            String distance = String.valueOf(distanceValue);
                            Friend f = new Friend(name, day, hourAtLocation, minuteAtLocation, email, distance, locationLat, locationLong);

                            String locat = f.lat + "," + f.lon;

                            try {
                                markers += "&markers=color:blue|" + URLEncoder.encode(locat, "utf-8");

                            } catch (UnsupportedEncodingException e) {
                                return;
                            }

                            updateList(f);


                        }//end loop
                        markerString += markers;
                        curLocString=cString;
                        d("markerSring b4", "string: " + markerString);

                        FriendListAdapter adapter = new FriendListAdapter(MainActivity.this,
                                R.layout.listview_row, friend_data);

                        ListView listView1 = (ListView) findViewById(R.id.listData);
                        listView1.setAdapter(adapter);

                        //fix error?
                        //adapter.notifyDataSetChanged();
                        //update map
                        int mapWidth = mapImage.getWidth();
                        int mapHeight = mapImage.getHeight();
                        //String query = "3200 Chestnut Street";

                        try {

                            String mapUrl = "http://maps.googleapis.com/maps/api/staticmap?center=" +
                                    URLEncoder.encode("3200 Chestnut Street", "utf-8") + markerString+ "&zoom=17&size=" + mapWidth + "x" + mapHeight + "&type=roadmap&sensor=false";
                            i(tag, mapUrl);

                            new ImageTask().execute(mapUrl);

                        } catch (UnsupportedEncodingException e) {

                            e.printStackTrace();
                        }
                    }


                });


        }
        }); //end on click


        //set up long click listener for email
        lv= (ListView)findViewById(R.id.listData);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long id) {

                String address = getEmail(pos);

                //send email
               sendEmail(address);

                return true;
            }
        });


    }

    public void updateList(Friend f) {
    friend_data.add(f);

    }
    //convert locations to miles
    public double coordinateToMiles(Location l1, Location l2){
        double lat1 = l1.getLatitude();
        double lon1 = l1.getLongitude();
        double lat2 = l2.getLatitude();
        double lon2 = l2.getLongitude();

        double x = 69.1 * (lat2 - lat1);
        double y = 53.0 * (lon2 - lon1);

    double distance = Math.sqrt(x * x + y * y);

        return distance;

    }
    public Location getLoc(){
        Location L = null;
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                new LocationListener() {
                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {
                    }

                    @Override
                    public void onProviderEnabled(String provider) {
                    }

                    @Override
                    public void onProviderDisabled(String provider) {
                    }

                    @Override
                    public void onLocationChanged(final Location location) {
                    }
                });

        Location currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return L; //return null
        }

      //  TextView txtLat;
        //  txtLat = (TextView) findViewById(R.id.textView);
       // txtLat.setText("Your latitude:" + currentLocation.getLatitude() + ", Your longitude:" + currentLocation.getLongitude());

        return currentLocation;
    }


    //helper
    public String getEmail(int index){
        if (index ==0)
            return "dannyp789@gmail.com";
        else
            return "dannyp789@gmail.com";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void sendEmail(String s){

        Log.d("email","Email sent to: " + s);
    }
    private class ImageTask extends AsyncTask<String, Void, Bitmap>{

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bmp = null;

            // Connect to the URL
            URL url;
            try {
                url = new URL(params[0]);
                HttpURLConnection request = (HttpURLConnection) url.openConnection();
                request.setRequestProperty("Content-Type", "application/octet-stream");
//				request.setRequestProperty("Expect", "100-continue");
                request.connect();

                bmp = BitmapFactory.decodeStream(((InputStream) request.getContent()));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap result){
            mapImage.setImageBitmap(result);
        }


    }

    private class EmailTask{

        //protected String doInBackground(String... params) {

            String s = "";

            //***SEND EMAIL
            /*
            new AsyncTask<Void, Void, Boolean>() {
                @Override public Boolean doInBackground(Void... arg) {
                    try {
                        Log.d("gmail pass", "pass is: "+ gmailpassword);
                        GMailSender sender = new GMailSender("dannyp789@gmail.com", gmailpassword);
                        sender.sendMail("This is Subject",
                                "This is Body",
                                "dannyp789@gmail.com",
                                "dannyp789@gmail.com");
                    } catch (Exception e) {
                        Log.e("SendMail", e.getMessage(), e);
                    }
                    return true;}
            }.execute();
            */


        }


}
