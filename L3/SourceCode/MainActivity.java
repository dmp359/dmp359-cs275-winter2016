package com.example.danmac.tictactoe;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Arrays;


public class MainActivity extends ActionBarActivity implements OnClickListener{

     int turn = 0; // Who's turn is it? 0=X 1=O
     int boardArray[][] = new int[3][3]; // x=0, O=1

    //global button variables
    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, rButton;
    Button[] buttonArray;

    String winningPosition="";

    TextView gameLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resetBoard();

    }
public void disableAllButtons()
{
    b1 = (Button) findViewById(R.id.Button1);
    b2= (Button) findViewById(R.id.Button2);
    b3= (Button) findViewById(R.id.Button3);
    b4= (Button) findViewById(R.id.Button4);
    b5= (Button) findViewById(R.id.Button5);
    b6= (Button) findViewById(R.id.Button6);
    b7= (Button) findViewById(R.id.Button7);
    b8= (Button) findViewById(R.id.Button8);
    b9= (Button) findViewById(R.id.Button9);

    //fill button array. bad for memory. oh well
    buttonArray = new Button[]{b1, b2, b3, b4, b5,b6, b7,b8,b9};

    //set on click event listeners
    for(Button b : buttonArray){
        b.setEnabled(false);

    }

}
    public void resetBoard()
    {
        b1 = (Button) findViewById(R.id.Button1);
        b2= (Button) findViewById(R.id.Button2);
        b3= (Button) findViewById(R.id.Button3);
        b4= (Button) findViewById(R.id.Button4);
        b5= (Button) findViewById(R.id.Button5);
        b6= (Button) findViewById(R.id.Button6);
        b7= (Button) findViewById(R.id.Button7);
        b8= (Button) findViewById(R.id.Button8);
        b9= (Button) findViewById(R.id.Button9);


        //fill button array. bad for memory. oh well
        buttonArray = new Button[]{b1, b2, b3, b4, b5,b6, b7,b8,b9};

        //set on click event listeners
        for(Button b : buttonArray){
            b.setEnabled(true);
            b.setOnClickListener(this);
            b.setText("");
            //reset color
            b.setBackgroundResource(android.R.drawable.btn_default);
        }

        rButton = (Button) findViewById(R.id.resetButton);
        rButton.setOnClickListener(this);


        //make them all -1s
        for (int[] row : boardArray)
            Arrays.fill(row, -1);


        turn=0;
        winningPosition="";
        gameLabel=(TextView)findViewById(R.id.gameInfo);
        gameLabel.setTextColor(Color.BLACK);

        gameLabel.setText("X's turn");
    }


    //On click
    @Override
    public void onClick(View v) {
        boolean wasValidMove=false;

        gameLabel = (TextView) findViewById(R.id.gameInfo);

        Button b = (Button) v;


        //check if it is the reset button
        if (b.getText().toString().equals("New Game")){
           resetBoard();
        }


        //Determine to set x or o
        if (b.getText().toString().equals("")) {

            if (turn == 0) {
                b.setText("X");
                gameLabel.setText("O's Turn");
                wasValidMove=true;
            }
            if (turn == 1) {
                b.setText("O");
                gameLabel.setText("X's Turn");
                wasValidMove=true;
            }
        }


        //Set grid appropriately depending on what was clicked if move was valid
        if (wasValidMove) {
            switch (v.getId()) {
                case R.id.Button1:
                    boardArray[0][0] = turn;
                    break;
                case R.id.Button2:
                    boardArray[0][1] = turn;
                    break;
                case R.id.Button3:
                    boardArray[0][2] = turn;
                    break;
                case R.id.Button4:
                    boardArray[1][0] = turn;
                    break;
                case R.id.Button5:
                    boardArray[1][1] = turn;
                    break;
                case R.id.Button6:
                    boardArray[1][2] = turn;
                    break;
                case R.id.Button7:
                    boardArray[2][0] = turn;
                    break;
                case R.id.Button8:
                    boardArray[2][1] = turn;
                    break;
                case R.id.Button9:
                    boardArray[2][2] = turn;
                    break;
            }
        }

        //Check for winner
        boolean gameOver = checkForWinner();
        if (gameOver) {
            //current turn value must be winner
            int winner = turn;

            if (winner == 0) {
                gameLabel.setTextColor(Color.RED);
                gameLabel.setText("X wins!");

            }
            if (winner == 1) {
                gameLabel.setTextColor(Color.RED);
                gameLabel.setText("O wins!");

            }


            lightUpButtons();

            disableAllButtons();
            return;

        }
        //no winner, so maybe tie?

        boolean wasTie = true;
        //Check for tie (all boardArray filled with values > -1)
        for (int i = 0; i< boardArray.length; i++ ){
            for (int j=0; j<boardArray[i].length; j++){
                if (boardArray[i][j]==-1) //not a tie
                    wasTie=false;
            }
        }
        //if we get here without hitting the if statement, wasTie is true.
        if (wasTie) {
            gameLabel.setTextColor(Color.RED);
            gameLabel.setText("Tie game!");
            //stop game
            disableAllButtons();

            return;
        }

        //not a tie, no winner, so update turn if move was valid
        if (wasValidMove) {
            if (turn == 0) {
                turn = 1;
                return; // return so the next statement isn't true
            }
            if (turn == 1) {
                turn = 0;
                return;
            }
        }
    }

    private void lightUpButtons() {
        //Given global winning position, light up the correct buttons
        b1 = (Button) findViewById(R.id.Button1);
        b2= (Button) findViewById(R.id.Button2);
        b3= (Button) findViewById(R.id.Button3);
        b4= (Button) findViewById(R.id.Button4);
        b5= (Button) findViewById(R.id.Button5);
        b6= (Button) findViewById(R.id.Button6);
        b7= (Button) findViewById(R.id.Button7);
        b8= (Button) findViewById(R.id.Button8);
        b9= (Button) findViewById(R.id.Button9);

        if (winningPosition.equals("row1")) {
            b1.setBackgroundColor(Color.YELLOW);
            b2.setBackgroundColor(Color.YELLOW);
            b3.setBackgroundColor(Color.YELLOW);
        }
        if (winningPosition.equals("row2")) {
            b4.setBackgroundColor(Color.YELLOW);
            b5.setBackgroundColor(Color.YELLOW);
            b6.setBackgroundColor(Color.YELLOW);
        }
        if (winningPosition.equals("row3")) {
            b7.setBackgroundColor(Color.YELLOW);
            b8.setBackgroundColor(Color.YELLOW);
            b9.setBackgroundColor(Color.YELLOW);
        }
        if (winningPosition.equals("col1")) {
            b1.setBackgroundColor(Color.YELLOW);
            b4.setBackgroundColor(Color.YELLOW);
            b7.setBackgroundColor(Color.YELLOW);
        }
        if (winningPosition.equals("col2")) {
            b2.setBackgroundColor(Color.YELLOW);
            b5.setBackgroundColor(Color.YELLOW);
            b8.setBackgroundColor(Color.YELLOW);
        }
        if (winningPosition.equals("col3")) {
            b3.setBackgroundColor(Color.YELLOW);
            b6.setBackgroundColor(Color.YELLOW);
            b9.setBackgroundColor(Color.YELLOW);
        }
        if (winningPosition.equals("diag1")) {
            b1.setBackgroundColor(Color.YELLOW);
            b5.setBackgroundColor(Color.YELLOW);
            b9.setBackgroundColor(Color.YELLOW);
        }
        if (winningPosition.equals("diag2")) {
            b7.setBackgroundColor(Color.YELLOW);
            b5.setBackgroundColor(Color.YELLOW);
            b3.setBackgroundColor(Color.YELLOW);
        }
    }

    public boolean checkForWinner(){

         //row 1
        if (boardArray[0][0] == turn && boardArray[0][1]==turn && boardArray[0][2]==turn) {
            winningPosition="row1";
            return true;
        }
        //row 2
         if (boardArray[1][0] == turn  && boardArray[1][1]== turn && boardArray[1][2]== turn){
            winningPosition="row2";
            return true;
        }
            //row 3
        else if (boardArray[2][0] == turn && boardArray[2][1]== turn && boardArray[2][2]== turn){
            winningPosition="row3";
        return true;
    }
            //column 1
        else if (boardArray[0][0] == turn && boardArray[1][0]== turn && boardArray[2][0]== turn){
            winningPosition="col1";
            return true;
        }
             //column 2
        else if (boardArray[0][1]== turn && boardArray[1][1]== turn && boardArray[2][1]== turn) {
            winningPosition="col2";
            return true;
        }
            //column 3
        else if (boardArray[0][2]== turn && boardArray[1][2]== turn && boardArray[2][2]== turn){
            winningPosition="col3";
            return true;
        }
            //diagonal 1
        else if (boardArray[0][0]== turn&& boardArray[1][1]== turn && boardArray[2][2]== turn){
            winningPosition="diag1";
            return true;
        }
        //diag 2
        else if (boardArray[2][0] == turn && boardArray[1][1]== turn && boardArray[0][2]== turn) {
            winningPosition = "diag2";
            return true;
        }
        else
            return false;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
