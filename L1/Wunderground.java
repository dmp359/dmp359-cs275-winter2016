import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.io.InputStream;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//Dan Perlman
public class Wunderground {

	public static void main(String[] args) throws IOException {
		
		//Check arguments
		
		String key;
		/*
		if(args.length < 1) {
		     System.out.println("Enter key: ");
		     Scanner in = new Scanner(System.in);
		     key = in.nextLine();
		} else {
		     key = args[0];
		}
		*/
		
		
		//my key
		key = "dc03cb46f317b319";
		//Declare url
		String sURL = "http://api.wunderground.com/api/" + key + "/geolookup/q/autoip.json";

		// Connect to the URL
		URL url = new URL(sURL);
		HttpURLConnection request = (HttpURLConnection) url.openConnection();
		request.connect();

		//Json parsing
		JsonParser jp = new JsonParser();
		JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));

		
		//go through the data-----
		
		//get the root object
		JsonObject rootobj = root.getAsJsonObject();
		
		//dive into the location object by first getting the location object
		JsonObject location = rootobj.get("location").getAsJsonObject();
		
		//using this location object, get the zip/city/state etc as strings
		String zip = location.get("zip").getAsString();
		String city = location.get("city").getAsString();
		String state = location.get("state").getAsString();

	
		
		//Print
		System.out.println("Your zip code is : " + zip);
		System.out.println("Your city is : " + city);
		System.out.println("Your state is : " + state);
		System.out.println("=======================================================||================================================");

		//PART 4. Hourly forecast
		//New URL
		String hourlyURL = "http://api.wunderground.com/api/" + key + "/hourly/q/autoip.json";

		// Connect to the URL
		URL hURL = new URL(hourlyURL);
		HttpURLConnection request2 = (HttpURLConnection) hURL.openConnection();
		request2.connect();

		//Json parsing
		JsonParser jp2 = new JsonParser();
		JsonElement root2 = jp2.parse(new InputStreamReader((InputStream) request2.getContent()));
		
		//get the root object
		JsonObject rootobj2 = root2.getAsJsonObject();
		//get the hourly forecast object
		JsonArray hourlyForecast = rootobj2.get("hourly_forecast").getAsJsonArray();
		
		//Data:
		//FCTTIME 
			//pretty (time and date)
		//Weather condition
			//temp in f
		//Current temperature
		//Relative humidity

		JsonObject fcttime;
		//loop through forecasts
		for (int i=0; i<hourlyForecast.size(); i++)
		{
			//get fcctime object
			 fcttime = hourlyForecast.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject();

			//get strings for date and time 
			String pretty = fcttime.get("pretty").getAsString();
			String condition = hourlyForecast.get(i).getAsJsonObject().get("condition").getAsString();
			String humidity = hourlyForecast.get(i).getAsJsonObject().get("humidity").getAsString();
			String temperature = hourlyForecast.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
			//print the strings
			System.out.format("%-40s%-20s%20s%25s", pretty, condition , " Humidity: " + humidity , "Temperature (f): " + temperature);			
			System.out.println();
		}
	}
		
		
}

	