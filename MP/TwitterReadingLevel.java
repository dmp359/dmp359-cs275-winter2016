import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;


public class TwitterReadingLevel
{
	//given a string,return false if it has a # or @, true otherwise. 
	public static boolean isAWord(String s)
	{
		boolean returnBool=true;
		for (int i=0; i<s.length(); i++)
		{
			if (s.charAt(i) == '@' || s.charAt(i) == '#' )
				returnBool=false;	
		}
		return returnBool;
	}
	//returns true if a string is a url, false otherwise
	public static boolean isAURL(String s)
	{
		boolean returnBool=true;
		String something;
		try {
		  URL url = new URL(s);
		} catch (MalformedURLException e) {
		  // it wasn't a URL
			returnBool=false;
		}
		return returnBool;
		
	}
	
	//returns a string without punctuation or any other characters besides letters
	public static String cleanUp(String s)
	{
		return s.replaceAll("[^a-zA-Z]", "");	
	}
	public static void main(String[] args) throws TembooException, IOException {
		String acctName, appKeyName, appKeyValue;
		String twitterKey, twitterSecret, twitterAccessToken, twitterTokenSecret;
		String twitterScreenName, wordKey;
		Scanner in = new Scanner(System.in);
		
		System.out.println("Entered Temboo Account Name");
		acctName="dmp359";
		System.out.println("Entered Temboo App Key Name");
		appKeyName = "twitterReadingLevel";
		System.out.println("Entered Temboo App Key Value");
		appKeyValue = "Dj1vTAsNzlKjGNErz1KdxsmU3pr04Nk9";
		System.out.println("Entered Twitter App key");
		twitterKey="6q7LqpKfPowRpKiVZ9R86Z1nq";
		System.out.println("Entered Twitter secret");
		twitterSecret="0Fyj2kUVq05wMVezSC6OqNNS7VAiOcpdQkzlwwGygiOc306zTz";
		System.out.println("Entered Twitter access token");
		twitterAccessToken="1656394422-Bvk5hghoNU1PFL2Jl2MLgmsyJxlDMtfXJCmnfXW";
		System.out.println("Entered Twitter access token secret");
		twitterTokenSecret="08YmYkhbbzPYlLV9relWBZZnDvoTNYM7dgsVQQetEPGtR";
	
		System.out.println("Entered wordnik API Key");
		wordKey="29fe338088656d553582b06026708512f224d7e3328d949a5";
		System.out.println("Enter screen name to analyze:");
		twitterScreenName = in.nextLine();

    	int numberOfTweets=30;

		
		//temboo session with twitter app name and key
		TembooSession session = new TembooSession(acctName, appKeyName, appKeyValue);

		
		//Get tweets from timeline
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_AccessToken(twitterAccessToken);
		userTimelineInputs.set_AccessTokenSecret(twitterTokenSecret);
		userTimelineInputs.set_ConsumerKey(twitterKey);
		userTimelineInputs.set_ConsumerSecret(twitterSecret);
		userTimelineInputs.set_ScreenName(twitterScreenName);
		//optional input. number of tweets
		userTimelineInputs.set_Count(numberOfTweets);
		
		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);

		String output = userTimelineResults.get_Response();
		
		
		// Convert to a JSON object to print data
    	JsonParser jparser = new JsonParser();
    	JsonElement jRoot = jparser.parse(userTimelineResults.get_Response());
    	JsonArray jRootArray = jRoot.getAsJsonArray(); 

    	
    	int numberOfPolysyllables=0;
    	for(int i = 0; i < jRootArray.size(); i++) {
    		String tweet = jRootArray.get(i).getAsJsonObject().get("text").getAsString();
    		//System.out.println(tweet);
    		
    		for (String word : tweet.split(" ")) {
    			if (isAWord(word))   //if word doesn't have # or &
    			{ 
    				if (!isAURL(word)){//if word is not a URL
    					//convert it to lowercase so we get more accurate results
    					word=word.toLowerCase();
    				
    					//clean up word. get rid of punctiation
    					word= cleanUp(word);
    				
    					if (word.equals(""))
    						break;
    					//check if multi syllabic
    					String wordNikURL = "http://api.wordnik.com:80/v4/word.json/" + word + "/hyphenation?useCanonical=false&limit=50&api_key="+wordKey;
    					// Connect to the URL
    					URL url = new URL(wordNikURL);
    					HttpURLConnection request = (HttpURLConnection) url.openConnection();
    					request.connect();
    					//Json parsing
    					JsonParser jp = new JsonParser();
    					JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
    				    				
    					//get the root array
    					JsonArray rootArray = root.getAsJsonArray();

    					//check if more than 2 syllables. if so, print the word and increase counter
    					if (rootArray.size() > 2){
    						System.out.println(word + " ---------- " + rootArray.size() + " syllables.");
    						numberOfPolysyllables++;
    					}
    				}
    			}
    			
    		}
    		

    	} //end of tweet array traversal
    	
    	//calculate grade
    	double grade = 1.0430 * Math.sqrt(numberOfPolysyllables) * (30/ numberOfTweets) + 3.1291;
    	System.out.println("Reading level is: " + grade);
    	
	}
}
