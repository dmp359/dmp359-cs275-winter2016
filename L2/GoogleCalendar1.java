import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.omg.CORBA.portable.OutputStream;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.*;
public class GoogleCalendar1 {
	
	
	
	    public static String executePost(String targetURL, String urlParameters) {
	        URL url;
	        HttpURLConnection connection = null;
	        try {
	            //Create connection
	            url = new URL(targetURL);
	            connection = (HttpURLConnection) url.openConnection();
	            connection.setRequestMethod("POST");
	            connection.setRequestProperty("Content-Type",
	                    "application/x-www-form-urlencoded");

	            connection.setRequestProperty("Content-Length", ""
	                    + Integer.toString(urlParameters.getBytes().length));
	            connection.setRequestProperty("Content-Language", "en-US");

	            connection.setUseCaches(false);
	            connection.setDoInput(true);
	            connection.setDoOutput(true);

	            //Send request
	            DataOutputStream wr = new DataOutputStream(
	                    connection.getOutputStream());
	            wr.writeBytes(urlParameters);
	            wr.flush();
	            wr.close();

	            //Get Response  
	            InputStream is = connection.getInputStream();
	            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	            String line;
	            StringBuilder response = new StringBuilder();
	            while ((line = rd.readLine()) != null) {
	                response.append(line);
	                response.append('\r');
	            }
	            rd.close();
	            return response.toString();

	        } catch (Exception e) {

	            e.printStackTrace();
	            return null;

	        } finally {

	            if (connection != null) {
	                connection.disconnect();
	            }
	        }
	    }
	
	public static int getLengthOfDate(String s){
		
		int counter=0;
		for (int i=0; i<s.length(); i++)
		{
			if (s.charAt(i) == 'T')
				break;
			else
				counter++;
			
		}
		
		return counter;
	}
	public static void main(String[] args) throws Exception {

			String consumerkey, consumersecret, code, redirect_uri;
				
			Scanner in = new Scanner(System.in);
			System.out.println("Enter Google Client ID:");
			consumerkey = in.nextLine();
			System.out.println("Enter Google Client Secret:");
			consumersecret = in.nextLine();
			System.out.println("Enter Google Redirect URI:");
			redirect_uri = in.nextLine();
			
			System.out.println("Go to the following website and get the code, entering that here\nhttps://accounts.google.com/o/oauth2/v2/auth?scope=https://www.googleapis.com/auth/calendar&redirect_uri="+redirect_uri+"&response_type=code&client_id=" + consumerkey);
			code = in.nextLine();

			String sURL = "https://accounts.google.com/o/oauth2/token";
			String params = "&code=" + code + "&client_id="+ consumerkey + "&client_secret=" + consumersecret+ "&redirect_uri=" + redirect_uri + "&grant_type=authorization_code";
			String authorizedRes= executePost(sURL, params);

			JsonParser oauth_jp = new JsonParser();
			JsonElement oauth_root = oauth_jp.parse(authorizedRes);
			JsonObject oauth_rootobj = oauth_root.getAsJsonObject(); // may be Json Array if it's an array, or other type if a primitive
			String accessToken = oauth_rootobj.get("access_token").getAsString();
		
			
			//Get list of cal urls
			String connectionURL1 = "https://www.googleapis.com/calendar/v3/users/me/calendarList?oauth_token=";
			connectionURL1+=accessToken;
			
			// Connect to the URL
			URL url = new URL(connectionURL1);
					
			HttpURLConnection request1 = (HttpURLConnection) url.openConnection();
			request1.connect();

			//print list of cals...
			System.out.println("Calendars: ");
			//Json parsing
			JsonParser jp = new JsonParser();
			JsonElement root = jp.parse(new InputStreamReader((InputStream) request1.getContent()));
			JsonObject rootObject = root.getAsJsonObject();
			
			JsonArray cals = rootObject.get("items").getAsJsonArray();
			String firstCal="",firstCalName="";
			for (int i=0; i<cals.size(); i++)
			{
				JsonObject cal = cals.get(i).getAsJsonObject();
				String id = cal.get("id").getAsString();
				String name = cal.get("summary").getAsString();
				//get first cal Id for later step
				if (i==0){
					firstCal = id;
					firstCalName = name;
				}
				System.out.println("Calendar ID: " + id + " | Calendar Name: " + name);
			}
			
			System.out.println("Showing events for the calendar named: '" + firstCalName + "'...");
		
			//see calendar 1's events
			String connectionURL2 = "https://www.googleapis.com/calendar/v3/calendars/";
			connectionURL2+=firstCal;
			connectionURL2+="/events?oauth_token="+accessToken;
					
			// Connect to the URL
			url = new URL(connectionURL2);
		
			HttpURLConnection request2 = (HttpURLConnection) url.openConnection();
			request2.connect();

			//Json parsing
			JsonParser jp2 = new JsonParser();
			JsonElement root2 = jp2.parse(new InputStreamReader((InputStream) request2.getContent()));
			JsonObject rootObject2 = root2.getAsJsonObject();
			
			JsonArray events = rootObject2.get("items").getAsJsonArray();
			for (int i=0; i<events.size(); i++)
			{
				//get event object
				JsonObject event = events.get(i).getAsJsonObject();
				 
				JsonObject sTime = event.get("start").getAsJsonObject();
				JsonObject eTime = event.get("end").getAsJsonObject();
				 
				 //Print date, time, and title
				 String startDate, endDate, startDateTime, endDateTime, title, startTime, endTime;
				 
				//event name
				title = event.get("summary").getAsString();
				
				//start date/time
				startDateTime = sTime.get("dateTime").getAsString();
				
				//end date/time
				endDateTime = eTime.get("dateTime").getAsString();
				
				 //seperate the date and time (by the character T) using an endpoint helper
				int endPoint1 = getLengthOfDate(startDateTime);
				int endPoint2 = getLengthOfDate(endDateTime);
				
				//start date (before T)
				startDate = startDateTime.substring(0,endPoint1);
				//start time (after T)
				startTime = startDateTime.substring(endPoint1+1, startDateTime.length());
				
				//end date (before T)
				endDate = endDateTime.substring(0,endPoint2);
				//end time (after T)
				endTime = endDateTime.substring(endPoint2+1, endDateTime.length());
				
				 System.out.println(title + " on " + startDate + " at " + startTime + " to " + endDate + " at " + endTime);

			}

					

	}
}

